# jhjljkj

a = 3  # int
b = 7

c = a / b  # div

g = a // b  # int part of div

r = b % a  # remainders

p = a ** b  # power

s = 'hello'
print(c, g, r, p, s, type(c), type(a), type(s))

g = 10 ** 1000

print(g)

# string
cat = 'a cat ate a dead rat'
print(cat[10:17:2])  # start:stop before:step
print(cat[::-1])  # start:stop before:step, - means backwards

# lists and tuples
l = [4, 5, 6]  # linst -- mutable
t = (7, 6, 8)  # tuple -- IMMUTABLE

print(type(l), type(t))

l[0] = 100500

print(l)

# t[0] = 100501
# print(t)

l.append(-100500)
print(l)

l.remove(100500)
print(l)

l = [1, 1.2, 'a cat a rat', (5, 1, 'cat rat')]  # list might take diff types
print('multi typed list')
print(l)

l.pop(0)  # remove by index
print('removed by index 0')
print(l)

# print('WRITE AN ____INTEGER______ NUMBER PLS')
# userInput = input()
# print(userInput, type(userInput))
#
# # casting
# print(int(userInput), type(int(userInput)))

# Boolean
# None type
print('' == None)
print([] == False)
print(0 == False)

# ref or value
a = 1
b = a
a = 2
print()
print(a, b)

m = [7, 6, 5]
n = m
m[0] = 'changed'
print(m, n)
