import using_classes_a


class Coder(using_classes_a.Person):
    def __init__(self, name, age, email, main_language):
        super().__init__(name, age, email)
        self.__main_language = main_language

    def __str__(self):
        return super().__str__() + f", my main language is {self.__main_language}"


if __name__ == '__main__':
    c = Coder('Grace', 84, 'grace@nasa.com', 'Python')
    print(c)
