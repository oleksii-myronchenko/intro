class Person:
    """
    This is a person, why not
    """

    def __init__(self, name, age, email):  # init method -- a constructor. Self is akin to 'this' in Java
        self.__name = name
        self.__age = age
        self.__email = email

    def print_me(self):  # all methods of a class must take self as a param
        print(f'Name: {self.__name}, Age: {self.__age}, Email: {self.__email}')

    def get_age(self):
        return self.__age

    def set_age(self, age):
        if int(float(age)) > 0:
            self.__age = age

    # akin to toString in Java
    def __str__(self):
        return f"I'm {self.__name}, I'm {self.__age} y.o. and my email is {self.__email}"

    # akin to hashCode? something that is used by the cli to disp an instance...
    def __repr__(self):
        return "I'm a silly overridden __repr__ that gives up no info"


if __name__ == '__main__':
    Ada = Person('Ada', 100500, 'ada.lovelace@royalmail.uk')
    Ada.print_me()
    Ada.set_age(1005001)  # happy birthday =)
    Ada.print_me()
    Ada.set_age('1005002')  # happy birthday =)
    Ada.print_me()
    print(Ada)
