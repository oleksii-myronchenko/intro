# decorators example
# Album class with id, title and content

class Album(object):
    def __init__(self, id, title, content):
        self.id = id  # will call the setter method
        self.__title = title
        self.__content = content

    @property  # the getter
    def id(self):
        return self.__id

    @id.setter  # the setter
    def id(self, new_id):
        if (int(float(new_id))) > 0:
            self.__id = new_id
        else:
            raise Exception('AAAAAAAAAAAAAAAAAA')

    @property
    def title(self):
        return self.__title

    @title.setter
    def title(self, value):
        self.__title = value

    @property
    def content(self):
        return self.__content

    @content.setter
    def content(self, value):
        self.__content = value

    def __str__(self):
        return f'THe id is "{self.id}" with the title "{self.title}" and contents "{self.content}"'


if __name__ == '__main__':
    a = Album(1, 'Cat mewling', 'When I ate a rat')
    print(a)
