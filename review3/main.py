import Weather
import CurrentWeather
import pickle

if __name__ == '__main__':
    w = Weather.Weather('AZ', 'Baku', 37.0543, 55, 17, 'SCORCHING')
    print(w)
    print(f'Temp in K:{w.temperature_kelvin}')
    print(f'Temp in F:{w.temperature_fahrenheit}')
    w.dump_to_file('weatherfile.txt')

    with open('weatherfile.txt', 'rb') as file:
        w1 = pickle.load(file)
        print(f'Weather from file: {w1}')

    cw = CurrentWeather.CurrentWeather('UA', 'Odesa', 42.3543, 96, 0, 'barely survivable')
    print(cw)
    cw1 = CurrentWeather.CurrentWeather('UA', 'Odesa', 42.3543, 96, 0, 'barely survivable')
    print(cw1)
