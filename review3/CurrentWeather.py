from datetime import datetime

import Weather


class CurrentWeather(Weather.Weather):
    def __init__(self, country_code, city, temperature, humidity, wind_speed, description):
        super().__init__(country_code, city, temperature, humidity, wind_speed, description)
        self.__datetime = datetime.now()

    @property
    def datetime(self):
        return self.__datetime

    def __str__(self):
        return f'{super().__str__()} \nTime: {self.datetime}'