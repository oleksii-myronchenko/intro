import requests


class countryCodeUtils:
    def __init__(self):
        #self.__countries = tuple(requests.get('https://api.first.org/data/v1/countries').json()['data'].keys())
        self.__countries = requests.get('http://country.io/names.json').json()
        self.__country_codes = tuple(self.__countries.keys())

    @property
    def countries(self):
        return self.__countries

    @property
    def country_codes(self):
        return self.__country_codes

    def validate_country_code(self, country_code):
        return country_code in self.__country_codes

    def country_name_by_code(self, country_code):
        return self.countries[country_code]


if __name__ == "__main__":
    countries = countryCodeUtils().countries
    print(countries)

    print(countryCodeUtils().validate_country_code('IE'))
    print(countryCodeUtils().validate_country_code('AZ'))
    print(countryCodeUtils().validate_country_code('GGG'))

    response = requests.get('https://api.first.org/data/v1/countries')
    print(response.json()['data'].keys())
