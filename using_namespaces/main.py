import furniture
from accountancy import table as acctable
import nested.nested as some_other_name
# modules from the python standard libs can be imported
import random
import math
# we can import libs from outside python but not by default (e.g. using pip)
import requests
# there is always a 'global' namespace for each module
g = "This is a global namespace outside any function"


def main():
    # make use of a global assets
    global g  # should be avoided ffs
    g = "altered"
    # make both tables
    f = furniture.table(40, 80)
    a = acctable(100, 20)
    n = some_other_name.table(100, 20, False)
    return [f, a, n]


if __name__ == '__main__':
    print(main())
    print(some_other_name.__name__)
    print(g)
