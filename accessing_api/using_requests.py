"""
This module uses the requests library to access end-point APIs on the Internets
It uses the exception handling to catch exceptions
"""
import requests  # pip3 install requests
import json


def getData(user_id):
    try:
        url = "http://dddddd"  # f'https://jsonplaceholder.typicode.com/users/{user_id}'
        results = requests.get(url)
        return json.dumps(results.json(), indent=4)
    except requests.ConnectionError as ce:
        print(f"there has been a Connection Exception {ce}")
    except Exception as e:
        print(f"there was an Exception {e}")



if __name__ == '__main__':
    print(getData(6))
    pass
