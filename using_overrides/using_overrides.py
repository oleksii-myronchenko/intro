# anything that is built in Python could be overridden
# including =, ==, +, - etc

class Word:
    """
    Compares words regardless of case
    e.g.Hello == hello is True
    """

    def __init__(self, text):
        self.__text = text

    @property
    def text(self):
        return self.__text

    def __eq__(self, other):
        return self.text.lower() == other.text.lower()


if __name__ == '__main__':
    w1 = Word('fff')

    w2 = Word('fFf')

    print(w1 == w2)
