# list, tuple, dictionary and set
# dictionary is non-indexed (not strictly true)
from typing import List, Any

d = {
    'name': 'cat',
    'ate': 'rat',
    'age': 100500,
    'member': True
}
print(d['name'])  # access keys using [] NOT A DOT DOT A NOT
d['age'] = 100501  # happy BDay cat!
print(d['age'])

print(d.keys())  # THAT'S WHY IT'S NOT STRICTLY TRUE
print(d.values())
print(type(d))

# some creation syntax
l = list((4, 3, 2))  # list from a tuple
t = tuple(l)  # new tuple copies stuff
d = dict()

print(l, t, d, type(t))

q = (5)
print(type(q))

q = (5,)
print(type(q))

# set is like a dict with no keys (and unique members    )
s = {'a', 'cat', 'ate', 'a', 'dead', 'rat'}
print(type(s), s.intersection(s), s)



