# functions are defd like this
from math import sqrt


def cube(x):
    return x ** 3


# return the hypotenuse
def hypotenuse(x=5, y=50):
    """
    The function returnong a hypothenuse of a square-angled triangle \n
    :param x: one cathet
    :param y: second cathet
    :return: hypothenuse value
    """
    return sqrt(x ** 2 + y ** 2)


# we can access args of any function
def my_args(*args):
    # the args collection will have all the args of the function. Args are a tuple
    print(args)


def my_kwargs(*args, **kwargs):  # any keyword arguments will exist in a dict
    print(args)
    print(kwargs)


if __name__ == '__main__':
    print(cube(3))
    print(cube(2))
    print(cube(100500))

    print(hypotenuse(3, 4))  # those are positional agrs
    print(hypotenuse(y=30, x=40))  # those are name args
    print(hypotenuse())  # default arg values

    my_args("a cat ate a dead rat", True, 0, ("cat", "ate", ["rat", ]))

    my_kwargs('cat', 'rat', name='cat', ate='rat', age=100500, l=[1, 2, 3], isAdmin=False)
