from math import sqrt
from random import randrange

winCondition = False

exitCommand = 'I\'m out'
clueCommand = 'help'
n = None
guessed_numbers = []


def prompt():
    """
    The function returning a standard prompt \n
    :return: Command prompt
    """
    print('to give up and exit type "' + exitCommand + '"')
    print('to have a clue type "' + clueCommand + '"')
    return input('type a number: ')


def get_clues(n):
    """
    The function that returns clues for a number \n
    :param n: a number
    :return: clues string
    """
    prime_numbers = [num for num in range(2, 101) if all(num % number != 0 for number in range(2, num))]
    odd = 2 % n == 1
    square = sqrt(n).is_integer()
    prime = prime_numbers.__contains__(n)
    return 'ODD: ' + str(odd), 'SQUARE: ' + str(square), 'PRIME: ' + str(prime)


def print_guess(lastGuess, diffRange):
    """
    Returns a guess value for a number (depends on a number and on a projected difference range) \n
    :param lastGuess:
    :param diffRange:
    :return:
    """
    print("Try " + str(lastGuess + diffRange))


if __name__ == '__main__':
    while True:
        winCondition = False
        if len(guessed_numbers) > 0:
            print("NEW ROUND BEGINS. LET'S GUESS MORE")
        numberToGuess = randrange(0, 100, 1)
        while not winCondition:
            n = prompt()
            if n.replace("-", "", 1).isnumeric():
                i = int(float(n))
                isInRange = 0 <= i < 100
                if isInRange:
                    if i < numberToGuess:
                        if numberToGuess - i < 3:
                            print('just a biiit higher')
                            print_guess(i, 1)
                        elif numberToGuess - i < 10:
                            print('bit higher')
                            print_guess(i, 4)
                        else:
                            print('that\'s too low...')
                            print_guess(i, 20)
                    elif i > numberToGuess:
                        if i - numberToGuess < 3:
                            print('just a biiit lower')
                            print_guess(i, -2)
                        elif i - numberToGuess < 10:
                            print('bit lower')
                            print_guess(i, -5)
                        else:
                            print('that\'s too high...')
                            print_guess(i, 20)
                    else:
                        print('You won, congrats')
                        guessed_numbers.append(numberToGuess)
                        winCondition = True
                else:
                    print('The number should be in [0, 100) range')
            elif n == exitCommand:
                if len(guessed_numbers) > 0:
                    print("Numbers guessed:", guessed_numbers)
                exit(0)
            elif n == clueCommand:
                print(get_clues(numberToGuess))
            else:
                print("You must type either a command or an integer")
