"""

"""
categories = ('users', 'posts', 'albums', 'todos', 'photos', 'todos')


def validate_id(the_id) -> bool:
    try:
        int(the_id)
        return True
    except Exception as e:
        print(f'validation for the id failed because of "{e}"')
        return False


def validate_category(category):
    return category in categories
