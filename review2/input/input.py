from .validation import validation

"""

"""


def input_category_and_user():
    try:
        print('Input the category')
        category = input()
        if not validation.validate_category(category):
            raise Exception(f"Validation for category failed because category should be in {validation.categories}")
        print('input the user id')
        user_id = input()
        if not validation.validate_id(user_id):
            raise Exception(f"Validation for id failed")
        return {'category': category, 'user_id': user_id}
    except Exception as e:
        print(f'Exception is caught: {e}')
