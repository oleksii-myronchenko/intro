def is_list_or_tuple(c):
    return type(c) == tuple or type(c) == list


def pretty_print_collection(c, indentation=0) -> str:
    '''
    Pretty prints collection containing primitives, lists or tuples
    :param c:
    :return:
    '''
    if not is_list_or_tuple(c):
        raise Exception("Tried to prettyPrint something other than a collection")
    if not type(indentation) == int:
        raise Exception("Indent should be int")
    result = ''
    for i in c:
        if is_list_or_tuple(i):
            result += f'\n{pretty_print_collection(i, indentation + 2)}'
        elif type(i) == dict:
            result += f'\n{pretty_print_dict(i, indentation + 2)}'
        else:
            result += f'{indent(indentation)}{str(i)}, '

    if result[len(result) - 1] == ' ' and result[len(result) - 2] == ',':
        result = result.rstrip(', ')
    return result


def pretty_print_dict(d, indentation=0) -> str:
    if type(d) != dict:
        raise Exception("Tried to prettyPrint something other than a dict")
    if not type(indentation) == int:
        raise Exception("Indent should be int")
    result = ""
    for k, v in d.items():
        if is_list_or_tuple(v):
            result += f'\n{str(k)}:\n{pretty_print_collection(v, indentation + 2)}'
        elif type(v) == dict:
            result += f'\n{str(k)}:\n{pretty_print_dict(v, indentation + 2)}'
        else:
            result += f"{indent(indentation)}{k} : {v} \n"
    return result


def indent(n):
    s = ""
    for n in range(0, n):
        s += " "
    return s


if __name__ == "__main__":
    print(pretty_print_collection(
        ['cat', 'ate', False, 'rat', 1, 1.0, [1, 2, 3], {'name': 'cat', 'ate': 'rat', 'some': False}]))

    print(pretty_print_dict(
        {'name': 'cat', 'ate': 'rat', 'some': False, 'list': [1, 2, 3], 'tuple': ('uno', 'dos', 'tres'),
         'dict': {'cat': 'name', 'rat': 'ate'}}))
