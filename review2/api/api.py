import requests


def get_category_by_user_id(category, user_id):
    """
    gets the category by user id
    :param category: 
    :param user_id: 
    :return: 
    """
    url = f"https://jsonplaceholder.typicode.com/{category}/{user_id}"
    return requests.get(url).json()
