import api.api as api
import input.input as user_input
import utils.utils as utils

'''

'''


def main(is_first):
    if not is_first:
        print('go again? y/n')
        go_again = input()
        if go_again == 'n':
            quit()

    user_data = user_input.input_category_and_user()
    json_response = api.get_category_by_user_id(user_data['category'], user_data['user_id'])
    print(utils.pretty_print_dict(json_response))


if __name__ == '__main__':
    is_first = True;
    while True:
        try:
            main(is_first)
            is_first = False
        except Exception as e:
            print(e)
