# using conditions
# logical conditions can branch decisions in code
# loops may ruin using 'for' or 'while'
i = None

while i != 0:
    n = input('enter a number ')
    if n.replace('.', '', 1).isnumeric():  # checks if a str can be cast to a number (or isDigit)
        i = int(float(n))
        if i < 0:
            print(n, 'is less than zero')
        elif i == 0:
            print(n, 'is zero')
        else:
            print(i, 'is more than 0')
    else:
        print('n is not a number')
