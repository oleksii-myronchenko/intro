# a generator will generate value
def make_gen(maximum):
    g = (num for num in range(0, maximum))  # returns a generator
    return g


def use_gen(j):
    for i in j:
        print(i, end=', ')


# can write custom gens
def my_gen(start=1, stop_before=10, step=2):  # start, stop-before and step for inc multiples
    '''
    Generates increasing multiples over range ov numbers
    :param start:
    :param stop_before:
    :param step:
    :return: the generatpr
    '''
    number = start
    while number < stop_before:
        yield number  # yield will return the next value in the generated sequence
        number *= step


if __name__ == "__main__":
    n = make_gen(1000)  # including 999 but not 1k
    print(n)
    #use_gen(n)

    for i in my_gen(1, 100500, 2):
        print(i, end=', ')
