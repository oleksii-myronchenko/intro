import doctest


def cube(x):
    """
    12weawe
    fwef
    awfe
    fe
    af
    
    >>> cube(3)
    27
    >>> cube(-1)
    -1
    """
    return x ** 3;


if __name__ == '__main__':
    doctest.testmod(verbose=True)
    # cube(3)  # expect 27
