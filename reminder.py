# import
import requests
from random import randint

if __name__ == "__main__":
    # using in
    t = ('posts', 'albums', 'photos')
    exists = 'posts' in t
    print(exists)
    print('users' in t)

    # iterate over a dict
    d = {'name': 'cat', 'ate': 'rat', 'admin': False, 'address': ['42', 'Censored pkwy', 'Streamstown']}
    for k, v in d.items():
        if type(v) == list:
            print(k, end=": \n")
            for item in d[k]:
                print(item, end=', ')
        else:
            print(k, v)

    # dict
