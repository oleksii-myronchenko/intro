# range and generators
num_l = [num for num in range(0, 100, 3)]  # start, stop-before, step
print(num_l)

# we can use generator as object
num_g = (num for num in range(0, 100, 3))
print(num_g, type(num_g))  # not a tuple, a GENERATOOOOAAR

# generator is to be iterated over
for i in num_g:
    print(i, end=', ')

# we often use a range to generate because the range numbers do not persist in memory
r = 0
for j in range(1, 1000):
    r += j

print('')
print(r)

# iterate over dicts (count letters in a string)
p = 'a cat ate a dead rat'
chars = {
    letter: p.count(letter) for letter in p
}
print(chars)

# make a gen for all the odd numbers up to 100
odd_num_g = (num for num in range(1, 100, 2))
for i in odd_num_g:
    print(i, end=', ')
