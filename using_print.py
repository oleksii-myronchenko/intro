# we can use print formatting with any output or string
def pretty_print(temperature=12, description='sunny', wind_speed=6.7):
    """
    The function returning the pretty printed version info
    :param temperature: temperature
    :param description: description
    :param wind_speed: speed of the wind
    :return: pretty weather string
    """
    formatted_string = "The weather is {} with a wind speed of {:0.2f} at {} degrees Celsius".format(description, wind_speed, temperature)
    # :0.2 is the floating point precision
#    formatted_string = "The weather is {0} with a wind speed of {1:0.2} at {2} degrees Celsius".format(d, s, t) # the same
#    formatted_string = f"The weather is {d} with a wind speed of {s:0.2} at {t} degrees Celsius" # the same
    print(formatted_string)
    pass


if __name__ == '__main__':
    pretty_print()
    pretty_print(35, 'SCORCHING', 0.11111)
